'''
It is happening

The machine Learns

Logistic Regression Implementation

Initial Training analysis will be basic

'''

import math
import numpy as np


'''
#Plot Sigmoid Function
h(z)=1/(1+e^-z)

z = b0 + b1*x1 + b2*x2


'''

def sigmoid(z):

	return 1/(1 + np.exp(-z))

#function [J, grad] = costFunction(theta, X, y)

def cost(theta, X, y):
	theta = np.matrix(theta)
	X = np.matrix(X)
	y = np.matrix(y)
	#print sigmoid(X * theta.T)
	dummy_matrix = np.dot(X,theta.T)
	for i in range(len(dummy_matrix)):
		if dummy_matrix[i] > 9:
			dummy_matrix[i] = 9
		elif dummy_matrix[i] < -9:
			dummy_matrix[i] = -9

	first = np.multiply(-y, np.log(sigmoid(dummy_matrix)))
	second = np.multiply((1 - y), np.log(1 - sigmoid(dummy_matrix)))

	#print np.sum(first - second) / (len(X))
	cost = np.sum(first - second) / (len(X))
	return np.sum(first - second) / (len(X))

def gradient(theta, X, y):
	theta = np.matrix(theta)
	X = np.matrix(X)
	y = np.matrix(y)
	
	parameters = int(theta.ravel().shape[1])
	grad = np.zeros(parameters)
	
	dummy_matrix = np.dot(X,theta.T)
	error = sigmoid(dummy_matrix) - y
	
	for i in range(parameters):
		term = np.multiply(error, X[:,i])
		grad[i] = np.sum(term) / len(X)
	#print grad
	return grad

def costReg(theta, X, y, learningRate):
	theta = np.matrix(theta)
	X = np.matrix(X)
	y = np.matrix(y)
	dummy_matrix = np.dot(X,theta.T)

	first = np.multiply(-y, np.log(sigmoid(dummy_matrix)))
	second = np.multiply((1 - y), np.log(1 - sigmoid(dummy_matrix)))
	reg = (learningRate / 2 * len(X)) * np.sum(np.power(theta[:,1:theta.shape[1]], 2))
	return np.sum(first - second) / (len(X)) + reg

def gradientReg(theta, X, y, learningRate):
	theta = np.matrix(theta)
	X = np.matrix(X)
	y = np.matrix(y)
	dummy_matrix = np.dot(X,theta.T)

	parameters = int(theta.ravel().shape[1])
	grad = np.zeros(parameters)
	
	error = sigmoid(dummy_matrix) - y
	
	for i in range(parameters):
		term = np.multiply(error, X[:,i])
		
		if (i == 0):
			grad[i] = np.sum(term) / len(X)
		else:
			grad[i] = (np.sum(term) / len(X)) + ((learningRate / len(X)) * theta[:,i])
	
	return grad

def map_feature(x1, x2, degree):
    '''
    Maps the two input features to quadratic features.
    Returns a new feature array with more features, comprising of
    X1, X2, X1 ** 2, X2 ** 2, X1*X2, X1*X2 ** 2, etc...
    Inputs X1, X2 must be the same size

    Taken from the internet and improved :)
    '''
    x1.shape = (x1.size, 1)
    x2.shape = (x2.size, 1)
    out = np.ones(shape=(x1[:, 0].size, 1))

    m, n = out.shape

    for i in range(1, degree + 1):
        for j in range(i + 1):
            r = (x1 ** (i - j)) * (x2 ** j)
            out = np.append(out, r, axis=1)

    return out






