'''

Example usage


'''
import sys
sys.path.insert(0, '../')

import numpy as np
import data_extract
import run
import log_reg1
import scipy.optimize

#Prediction helper function
def predict(theta, X):

	probability = log_reg1.sigmoid(np.dot(X,theta.T))

	return [1 if x >= 0.5 else 0 for x in probability]

#Check how many are correct
def correct(predictions, y):
	count = 0.0
	for i in range(y.shape[0]):
		if y[i] == predictions[i]:
			count = count + 1
	
	return count



#ML EX 2 Dataset 1 Section as a demo Example -----------------------------------------------------------------

print 'ML EX2 Dataset 1 Test'
X,y = data_extract.ML_EX2_Data1()
X = data_extract.add_ones_col(X)
X, norm = data_extract.normalize(X)

theta = np.zeros(X.shape[1])

result = scipy.optimize.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
theta_min=result[0]

predictions = np.array(predict(theta_min, X))
score = correct(predictions, y)

print ('This means that on the ML EX2 Dataset 1 that: ' +str(int(score)) + ' of ' + str(y.shape[0]) + 
	' or ' + str(score/float(y.shape[0])*100) + '% were correctly estimated')

#ML EX 2 Dataset 2 Section as a demo Example -----------------------------------------------------------------

print ''
print ''
print 'ML EX2 Dataset 2 Test'
X,y = data_extract.ML_EX2_Data2()
X = data_extract.add_ones_col(X)


#Map to higher features
degree = 1
X = log_reg1.map_feature(X[:,1],X[:,2],degree)
X, norm = data_extract.normalize(X)
theta = np.zeros(X.shape[1])
result = scipy.optimize.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
theta_min=result[0]

predictions = np.array(predict(theta_min, X))
score = correct(predictions, y)
print ('Test with a : ' + str(degree) + ' degree polynomial had ' +str(int(score)) + ' of ' 
	+ str(y.shape[0]) + ' or ' + str(score/float(y.shape[0])*100) + '% were correctly estimated')


#Beacon Data Section --------------------------------------------------------------------------

print ''
print ''
print 'Beacon Dataset'
print 'Basic Test'

X,y = data_extract.beacon_data()
X = data_extract.add_ones_col(X)


#Map to higher features
#degree = 4
#X = log_reg1.map_feature(X[:,1],X[:,2],degree)
X, norm = data_extract.normalize(X)
theta = np.zeros(X.shape[1])
result = scipy.optimize.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
theta_min=result[0]

predictions = np.array(predict(theta_min, X))
score = correct(predictions, y)
print ('Test with a : ' + str(degree) + ' degree polynomial had ' +str(int(score)) + ' of ' 
	+ str(y.shape[0]) + ' or ' + str(score/float(y.shape[0])*100) + '% were correctly estimated on the Beacon dataset')


#Mimic output -------------------------------------------------------------------------------------

output_predictions = log_reg1.sigmoid(np.dot(X,theta_min))

for i in range(len(output_predictions)):
	if y[i] == 1.0:
		print 'Estimate of ' + str(output_predictions[i]*100) + '% probability, actual is a WIN'
	else:
		print 'Estimate of ' + str(output_predictions[i]*100) + '% probability, actual is a LOSS'

print theta_min
print norm
