import sys
sys.path.insert(0, '../')


import numpy as np
from numpy import linspace, zeros, array, ones
import pandas as pd
import matplotlib.pyplot as plt
import log_reg1
import scipy.optimize as opt
import run

#matplotlib inline
import os

def predict(theta, X):
	probability = log_reg1.sigmoid(X * theta.T)

	return [1 if x >= 0.5 else 0 for x in probability]




data=[]

graph_me = []
y_win=[]
x_win=[]
y_loss = []
x_loss = []
X = []
y = []

deals = run.Deal.query.all()

for deal in deals:
    fee = int(deal.deal_fee)
    risk = deal.company_risk_rating
    risk_int = 0

    if risk == '1A':
        risk_int = 1
    elif risk == '1B':
        risk_int = 2
    elif risk == '1C':
        risk_int = 3    
    elif risk == '2A':
        risk_int = 4
    elif risk == '2B':
        risk_int = 5
    elif risk == '2C':
        risk_int = 6
    elif risk == '3A':
        risk_int = 7    
    elif risk == '3B':
        risk_int = 8
    elif risk == '3C':
        risk_int = 9
    elif risk == '4A':
        risk_int = 10
    elif risk == '4B':
        risk_int = 11   
    elif risk == '4C':
        risk_int = 12
    elif risk == '5A':
        risk_int = 13
    elif risk == '5B':
        risk_int = 14   
    elif risk == '5C':
        risk_int = 15
    else:
        risk_int = 16

    if deal.deal_status == "Win":
        y_win.append(float(fee))
        x_win.append(float(risk_int))
        y.append(1.0)
        data.append([1,1,fee,1])


    else:
        y_loss.append(fee)
        x_loss.append(risk_int)
        y.append(0.0)
        data.append([1,0,fee,0])






#------------- Hail Baby
'''
positive = data[data['Admitted'].isin([1])]
negative = data[data['Admitted'].isin([0])]

fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(positive['Exam 1'], positive['Exam 2'], s=50, c='b', marker='o', label='Admitted')
ax.scatter(negative['Exam 1'], negative['Exam 2'], s=50, c='r', marker='x', label='Not Admitted')

ax.legend()
ax.set_xlabel('Exam 1 Score')
ax.set_ylabel('Exam 2 Score')
'''

#plt.show()


# add a ones column - this makes the matrix multiplication work out easier
#data.insert(0, 1)

data = np.array(data)
print data
#data.insert(0,1)

# set X (training data) and y (target variable)
y = data[:,3:4]

X = data[:,0:3]
#print X
print y

print 'nick function time'
import data_extract
X,y = data_extract.beacon_data()
X = data_extract.add_ones_col(X)

print y

# convert to numpy arrays and initalize the parameter array theta
#X = np.array(X.value)
#y = np.array(y.value)
theta = np.zeros(3)

print X.shape
print theta.shape
print y.shape

print log_reg1.cost(theta,X,y)
print log_reg1.gradient(theta, X, y)

#Optimize Time

result = opt.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
print result
print log_reg1.cost(result[0],X,y)

theta_min = np.matrix(result[0])
print theta_min

predictions = predict(theta_min, X)
print predictions

correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0)) else 0 for (a, b) in zip(predictions, y)]
accuracy = (sum(map(int, correct)) % len(correct))
print accuracy
print 'accuracy = {0}%'.format(accuracy)

count = 0
for i in range(len(correct)):
    if correct[i] == 1:
        count = count + 1

print str(count) + ' out of ' + str(len(correct))

print X

'''

print 'intercepts'
print theta_min.item((0,1))

print 'x: ' + str(-theta_min.item((0,0))/theta_min.item((0,1)))
print 'y: ' + str(-theta_min.item((0,0))/theta_min.item((0,2)))


x1, y1 = [-theta_min.item((0,0))/theta_min.item((0,1)), 0], [0, -theta_min.item((0,0))/theta_min.item((0,2))]
ax.plot(x1, y1, marker = 'o')
#plt.show()
'''

