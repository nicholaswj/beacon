'''
Real Data
'''

import sys
sys.path.insert(0, '../')

import numpy as np
from numpy import linspace, zeros, transpose
import pandas as pd
import matplotlib.pyplot as plt
import log_reg1
import scipy.optimize as opt
import run
from run import db

#matplotlib inline
import os

def predict(theta, X):
	dummy_matrix = np.dot(X,theta.T)
	for i in range(len(dummy_matrix)):
		if dummy_matrix[i] > 9:
			dummy_matrix[i] = 9
		elif dummy_matrix[i] < -9:
			dummy_matrix[i] = -9
	probability = log_reg1.sigmoid(dummy_matrix)


	return [1 if x >= 0.5 else 0 for x in probability]

'''

Graphing Section 


'''


graph_me = []
y_win=[]
x_win=[]
y_loss = []
x_loss = []
X = []
y = []

deals = run.Deal.query.all()

for deal in deals:
	fee = int(deal.deal_fee)
	risk = deal.company_risk_rating
	risk_int = 0

	if risk == '1A':
		risk_int = 1
	elif risk == '1B':
		risk_int = 2
	elif risk == '1C':
		risk_int = 3	
	elif risk == '2A':
		risk_int = 4
	elif risk == '2B':
		risk_int = 5
	elif risk == '2C':
		risk_int = 6
	elif risk == '3A':
		risk_int = 7	
	elif risk == '3B':
		risk_int = 8
	elif risk == '3C':
		risk_int = 9
	elif risk == '4A':
		risk_int = 10
	elif risk == '4B':
		risk_int = 11	
	elif risk == '4C':
		risk_int = 12
	elif risk == '5A':
		risk_int = 13
	elif risk == '5B':
		risk_int = 14	
	elif risk == '5C':
		risk_int = 15
	else:
		risk_int = 16

	if deal.deal_status == "Win":
		y_win.append(float(fee))
		x_win.append(float(risk_int))
		y.append(1.0)

	else:
		y_loss.append(fee)
		x_loss.append(risk_int)
		y.append(0.0)


	X.append([1.0,float(risk_int),fee/100000.0])
	

fig = plt.figure()
ax = plt.subplot(111)

#plt.ylabel('Closing Fee')
plt.xlabel('Risk Rating')
plt.ylabel('Closing Fee')

ax.plot(x_win,y_win,'ro',label='Win')
ax.plot(x_loss,y_loss,'bo',label='Loss')

# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0 + box.width * 0.1, box.y0, box.width * 0.8, box.height])

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

plt.show()




'''

Machine learning Section

'''

X = np.array(X)
y = np.array(y)

print X

#111X3, 3X1
theta = np.zeros(3)

print 'cost and grad'
print log_reg1.cost(theta,X,y)
print log_reg1.gradient(theta, X, y)

print np.dot(X,theta)

learningRate = 0

print log_reg1.costReg(theta, X, y, learningRate)
print log_reg1.gradientReg(theta, X, y, learningRate)

#result = opt.fmin_tnc(func=log_reg1.costReg, x0=theta, fprime=log_reg1.gradientReg, args=(X, y, learningRate))
result = opt.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
print result

#Checker ---- still messy

theta_min = np.array(result[0])

X = np.array(X)
y = np.array(y)
theta = np.array((0,0,0))

print X.shape
print theta.shape
print y.shape

print log_reg1.cost(theta,X,y)
print log_reg1.gradient(theta, X, y)

#Optimize Time

result = opt.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
print result
print log_reg1.cost(result[0],X,y)

theta_min = np.matrix(result[0])

predictions = predict(theta_min, X)
correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0)) else 0 for (a, b) in zip(predictions, y)]
accuracy = (sum(map(int, correct)) % len(correct))
print 'accuracy = {0}%'.format(accuracy)

