import numpy as np
from numpy import linspace, zeros
import pandas as pd
import matplotlib.pyplot as plt
import log_reg1
import scipy.optimize as opt

#matplotlib inline
import os

def predict(theta, X):
	probability = log_reg1.sigmoid(X * theta.T)

	return [1 if x >= 0.5 else 0 for x in probability]





path = os.getcwd() + '/ex2data1.txt'
data = pd.read_csv(path, header=None, names=['Exam 1', 'Exam 2', 'Admitted'])
data.head()


positive = data[data['Admitted'].isin([1])]
negative = data[data['Admitted'].isin([0])]

fig, ax = plt.subplots(figsize=(12,8))

print data
data.insert(0, 'Ones', 1)
print data

# set X (training data) and y (target variable)
cols = data.shape[1]
X = data.iloc[:,0:cols-1]
y = data.iloc[:,cols-1:cols]

X.insert(3, 'mult', 1)

X_norm = X
y_norm = y

'''
for i in range(len(X_norm_1[0])):
	X_norm_1[3,i]=X_norm_1[3,1]*X_norm_1[3,2]

print X_norm_1
'''

# convert to numpy arrays and initalize the parameter array theta
X = np.array(X.values)
y = np.array(y.values)
theta = np.zeros(4)


for i in range(len(X)):
	X[i,3] = X[i,1]*X[i,2]
	#X[i,3] = 0.5

print X

print log_reg1.cost(theta,X,y)
print log_reg1.gradient(theta, X, y)

#Optimize Time

result = opt.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
print result
print log_reg1.cost(result[0],X,y)

theta_min = np.matrix(result[0])

predictions = predict(theta_min, X)
correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0)) else 0 for (a, b) in zip(predictions, y)]
accuracy = (sum(map(int, correct)) % len(correct))
print 'accuracy = {0}%'.format(accuracy)


print 'intercepts'
print theta_min.item((0,1))

print 'x: ' + str(-theta_min.item((0,0))/theta_min.item((0,1)))
print 'y: ' + str(-theta_min.item((0,0))/theta_min.item((0,2)))

#Now let's use pre_built model


from sklearn import metrics
from sklearn.linear_model import LogisticRegression
# load the iris datasets
# fit a logistic regression model to the data
model = LogisticRegression(fit_intercept=False, solver='liblinear',penalty='l1', C = 1e9)

model.fit(X_norm, y_norm['Admitted'])
print(model)

# make predictions
expected = y_norm
predicted = model.predict(X_norm)
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))

print model.coef_

new_theta = model.coef_
print type(new_theta)
theta_min[0,0] = new_theta[0,0]
theta_min[0,1] = new_theta[0,1]
theta_min[0,2] = new_theta[0,2]
theta_min[0,3] = new_theta[0,3]


print type(theta_min)



predictions = predict(theta_min, X)
correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0)) else 0 for (a, b) in zip(predictions, y)]
accuracy = (sum(map(int, correct)) % len(correct))
print 'accuracy = {0}%'.format(accuracy)




