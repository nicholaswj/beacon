import numpy as np
from numpy import linspace, zeros
import pandas as pd
import matplotlib.pyplot as plt
import log_reg1
import scipy.optimize as opt

#matplotlib inline
import os

def predict(theta, X):
	probability = log_reg1.sigmoid(X * theta.T)

	return [1 if x >= 0.5 else 0 for x in probability]



'''

path = os.getcwd() + '/ex2data1.txt'
data = pd.read_csv(path, header=None, names=['Exam 1', 'Exam 2', 'Admitted'])
data.head()


positive = data[data['Admitted'].isin([1])]
negative = data[data['Admitted'].isin([0])]
'''
fig, ax = plt.subplots(figsize=(12,8))
'''
ax.scatter(positive['Exam 1'], positive['Exam 2'], s=50, c='b', marker='o', label='Admitted')
ax.scatter(negative['Exam 1'], negative['Exam 2'], s=50, c='r', marker='x', label='Not Admitted')

ax.legend()
ax.set_xlabel('Exam 1 Score')
ax.set_ylabel('Exam 2 Score')
'''

#plt.show()
import data_extract

X,y = data_extract.ML_EX2_Data1()
X = data_extract.add_ones_col(X)
print X
X, dummy = data_extract.normalize(X)
print X

# add a ones column - this makes the matrix multiplication work out easier
#data.insert(0, 'Ones', 1)

# set X (training data) and y (target variable)
#cols = data.shape[1]
#X = data.iloc[:,0:cols-1]
#y = data.iloc[:,cols-1:cols]

X_norm = X
y_norm = y

# convert to numpy arrays and initalize the parameter array theta
#X = np.array(X.values)
#y = np.array(y.values)
theta = np.zeros(3)

print X.shape
print theta.shape
print y.shape

print log_reg1.cost(theta,X,y)
print log_reg1.gradient(theta, X, y)

#Optimize Time

result = opt.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
print result
print log_reg1.cost(result[0],X,y)

theta_min = np.matrix(result[0])

predictions = predict(theta_min, X)
correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0)) else 0 for (a, b) in zip(predictions, y)]
accuracy = (sum(map(int, correct)) % len(correct))
print 'accuracy = {0}%'.format(accuracy)


print 'intercepts'
print theta_min.item((0,1))

print 'x: ' + str(-theta_min.item((0,0))/theta_min.item((0,1)))
print 'y: ' + str(-theta_min.item((0,0))/theta_min.item((0,2)))


x1, y1 = [-theta_min.item((0,0))/theta_min.item((0,1)), 0], [0, -theta_min.item((0,0))/theta_min.item((0,2))]
#ax.plot(x1, y1, marker = 'o')
#plt.show()


#Phase 2 ------------------ 

#Plot
path = os.getcwd() + '/ex2data2.txt'
data2 = pd.read_csv(path, header=None, names=['Test 1', 'Test 2', 'Accepted'])



positive = data2[data2['Accepted'].isin([1])]
negative = data2[data2['Accepted'].isin([0])]



fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(positive['Test 1'], positive['Test 2'], s=50, c='b', marker='o', label='Accepted')
ax.scatter(negative['Test 1'], negative['Test 2'], s=50, c='r', marker='x', label='Rejected')
ax.legend()
ax.set_xlabel('Test 1 Score')
ax.set_ylabel('Test 2 Score')
#plt.show()

#Create Polynomial Functions
degree = 1
x1 = data2['Test 1']
x2 = data2['Test 2']

data2.insert(3, 'Ones', 1)

for i in range(1, degree+1):
	for j in range(0, i+1):
		data2['F' + str(i) + str(j)] = np.power(x1, i-j) * np.power(x2, j)

data2.drop('Test 1', axis=1, inplace=True)
data2.drop('Test 2', axis=1, inplace=True)

#data2.head()
print data2

# set X and y (remember from above that we moved the label to column 0)
cols = data2.shape[1]
X2 = data2.iloc[:,1:cols]
y2 = data2.iloc[:,0:1]

# convert to numpy arrays and initalize the parameter array theta
X2 = np.array(X2.values)
y2 = np.array(y2.values)
#theta2 = np.zeros(degree*(degree+1)/2)
theta2 = np.zeros(3)


learningRate = 0

print log_reg1.costReg(theta2, X2, y2, learningRate)
print log_reg1.gradientReg(theta2, X2, y2, learningRate)

result2 = opt.fmin_tnc(func=log_reg1.costReg, x0=theta2, fprime=log_reg1.gradientReg, args=(X2, y2, learningRate))


print result2

print log_reg1.costReg(result2[0],X2,y2,learningRate)


theta_min = np.matrix(result2[0])
predictions = predict(theta_min, X2)
correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0)) else 0 for (a, b) in zip(predictions, y2)]
accuracy = (sum(map(int, correct)) % len(correct))
print 'accuracy = {0}%'.format(accuracy)

'''
x3=np.arange(-1,2,0.1)
y3=x3

z3 = np.zeros((len(x3),len(y3)))
z3 = np.matrix((len(x3),len(y3)))
print z3
temp_poly = np.zeros(degree*(degree+1)/2)


for n in range(x3.size):
	for p in range(y3.size):

		temp_poly = np.zeros(degree*(degree+1)/2)
		print temp_poly
		count = 1
		temp_poly[0] = 1

		for i in range(1, degree):
			for j in range(0, i+1):
				val = np.power(x3[n], i-j) * np.power(y3[p], j)
				temp_poly[count] = val
				count = count + 1

		temp_sum = 0
		for val in temp_poly:
			temp_sum = temp_sum + val


		z3[n,p] = predict(theta_min, temp_poly)

CS = plt.contour(x3,y3,z3,10)

plt.show()
'''



def map_feature(x1, x2):
    '''
    Maps the two input features to quadratic features.
    Returns a new feature array with more features, comprising of
    X1, X2, X1 ** 2, X2 ** 2, X1*X2, X1*X2 ** 2, etc...
    Inputs X1, X2 must be the same size
    '''
    x1.shape = (x1.size, 1)
    x2.shape = (x2.size, 1)
    out = np.ones(shape=(x1[:, 0].size, 1))

    m, n = out.shape

    for i in range(1, degree + 1):
        for j in range(i + 1):
            r = (x1 ** (i - j)) * (x2 ** j)
            out = np.append(out, r, axis=1)

    return out


u = linspace(-1, 1.5, 200)
v = linspace(-1, 1.5, 200)
z = zeros(shape=(len(u), len(v)))
for i in range(len(u)):
    for j in range(len(v)):
        #z[i, j] = (map_feature(np.array(u[i]), np.array(v[j])).dot(np.array(theta_min.T)))
        predict_3 = log_reg1.sigmoid((map_feature(np.array(u[i]), np.array(v[j])).dot(np.array(theta_min.T))))
        if predict_3 > 0.5:
        	z[i,j] = 1
        else:
           	z[i,j] = 0


   		


z = z.T
CS = plt.contour(u, v, z)
'''
title('lambda = %f' % l)
xlabel('Microchip Test 1')
ylabel('Microchip Test 2')
legend(['y = 1', 'y = 0', 'Decision boundary'])
'''
#print z
'''
print X_norm
print y_norm['Admitted']

from sklearn import metrics
from sklearn.linear_model import LogisticRegression
# load the iris datasets
# fit a logistic regression model to the data
model = LogisticRegression()
model.fit(X_norm, y_norm['Admitted'])
print(model)

# make predictions
expected = y_norm
predicted = model.predict(X_norm)
print expected
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))

print model.coef_

'''
