'''
Test

for our Functions

Totally copied from Ex2 on Andrew Ng's Machine Learning Course
Just translating Octave to Python so I know my functions work

'''
import matplotlib.pyplot as plt
import csv
import log_reg

X =[]
y = []

win_list = []
loss_list = []


win_list_mod=[[],[]]
loss_list_mod=[[],[]]

f = open('ex2data1.txt', 'rt')
try:
    reader = csv.reader(f)
    for row in reader:
    	X.append([float(row[0]), float(row[1])] )
    	y.append(float(row[2]))
finally:
    f.close()

#Plot the initia linfo
fig = plt.figure()
ax = plt.subplot(111)

#plt.ylabel('Closing Fee')
plt.xlabel('Test 1 Score')
plt.ylabel('Test 2 Score')


#Gotta get into the numpy bro
for i in range(len(X)):
	if int(y[i]) == 1:
		win_list.append([X[i][0],X[i][1]])
	else:
		loss_list.append([X[i][0],X[i][1]])

for i in range(len(win_list)):
	win_list_mod[0].append(win_list[i][0])
	win_list_mod[1].append(win_list[i][1])

for i in range(len(loss_list)):
	loss_list_mod[0].append(loss_list[i][0])
	loss_list_mod[1].append(loss_list[i][1])


ax.plot(win_list_mod[0],win_list_mod[1],'ro',label='Admitted')
ax.plot(loss_list_mod[0],loss_list_mod[1],'bo',label='Rejected')


# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0 + box.width * 0.1, box.y0, box.width * 0.8, box.height])

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

#plt.show()

#continuing onwards
m = len(X)

#Add an intercept to X values so [1, x1, x2] vs [x1, x2]
x_new = []
temp = []
for i in range(len(X)):
	temp = [1]

	for n in range(len(X[0])):
		temp.append(X[i][n])

	x_new.append(temp)

X = x_new

#print X
n = len(X[0])

#Initialize Theta
initial_theta = [0]*(n)
print initial_theta
initial_theta = [-50,10,10]


#[cost, grad] = costFunction(initial_theta, X, y);

J, grad = log_reg.costFunction(initial_theta, X, y)
print 'Initial Cost: ' + str(J)
print 'Gradient: ' + str(grad)


updated_theta = log_reg.gradient_descent(initial_theta, X, y, 0.5, 4)
print 'Updated Thetas: ' + str(updated_theta)

J, grad = log_reg.costFunction(updated_theta, X, y)
print 'New Cost: ' + str(J)
print 'New Gradient: ' + str(grad)


print 'test accuracy'

temp = 0.0
test = 0.0
for i in range(len(X)):
	test = 0.0

	test = updated_theta[0] + X[i][1]*updated_theta[1]+ X[i][2]*updated_theta[2]

	if log_reg.sigmoid(test) >= 0.5 and int(y[i]) == 1:
		temp = temp+1
	elif log_reg.sigmoid(test) < 0.5 and int(y[i]) == 0:
		temp = temp+1

print temp

print 'intercepts'
print 'x: ' + str(-updated_theta[0]/updated_theta[1])
print 'y: ' + str(-updated_theta[0]/updated_theta[2])


x1, y1 = [-updated_theta[0]/updated_theta[1], 0], [0, -updated_theta[0]/updated_theta[2]]
#ax.plot(x1, y1, marker = 'o')
plt.show()






