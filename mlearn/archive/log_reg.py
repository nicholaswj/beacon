'''
It is happening

The machine Learns

Logistic Regression Implementation

Initial Training analysis will be basic

Inputs are (1 + x1 + x2)
x1 = Risk Rating (converted to a score 1-16)
x2 = closing fee

need to create:

y = {0 (win),1 (loss)}

h(z)=1/(1+e^-z)

z = b0 + b1*x1 + b2*x2



'''

import math
import numpy as np


'''
#Plot Sigmoid Function
h(z)=1/(1+e^-z)

z = b0 + b1*x1 + b2*x2


'''

def sigmoid(z):

	return 1/(1 + np.exp(-z))

#function [J, grad] = costFunction(theta, X, y)

def costFunction(theta, X, y):
	#Yeeeeah we gotta switch to lin Alg at some point

	J = 0.0
	grad = [None] * len(theta)

	m = len(y) #number of training sets

	temp_sum = 0.0 #used to sum J in the loop
	temp = 0.0 #within loop calc of J
	temp_thetax = 0.0 #theta(T)*x helper eqn
	temp_thetax_sum = 0.0 #theta(T)*x sum


	for i in range(len(y)):
		temp_thetax_sum = 0.0

		#get theta(T)*x
		for n in range(len(theta)):
			temp_thetax = theta[n] * X[i][n]
			temp_thetax_sum = temp_thetax_sum + temp_thetax

			#Quick if to protect from log(0) being calculated which throws an error
			if sigmoid(float(temp_thetax_sum)) > 0.99:
				temp = -y[i]*math.log(sigmoid(float(temp_thetax_sum)))-(1-y[i])*math.log(0.0001)
			else:
				temp = -y[i]*math.log(sigmoid(float(temp_thetax_sum)))-(1-y[i])*math.log(1.0-sigmoid(float(temp_thetax_sum)))

		temp_sum = temp_sum + temp 

	J = temp_sum/float(m)

	#Grad Function

	temp_grad_sum = 0.0
	temp_grad_partial = 0.0

	for j in range(len(theta)):

		temp_grad_partial = 0.0

		for i in range(m):
			temp_thetax_sum = 0.0

			#get theta(T)*x
			for n in range(len(theta)):
				temp_thetax = theta[n] * X[i][n]
				temp_thetax_sum = temp_thetax_sum + temp_thetax		

			h0 = sigmoid(temp_thetax_sum)

			temp = (h0-y[i])*X[i][j]

			temp_grad_partial = temp_grad_partial + temp


		grad[j] = temp_grad_partial/m


	return J, grad


def gradient_descent(initial_theta, X, y, alpha, iter):

	new_theta = []*len(initial_theta)

	current_theta = initial_theta

	print 'in improvement loop'

	for j in range(iter):
		J, grad = costFunction(current_theta, X, y)
		print ''
		print 'Cost: ' + str(J)
		print 'Gradient: ' + str(grad)
		print 'thetas: ' + str(current_theta)
		print 'alpha: ' + str(alpha)

		for i in range(len(current_theta)):
			print 'current theta: ' + str(current_theta[i])
			print 'alpha x grad: ' + str(alpha*grad[i])
			current_theta[i] = current_theta[i] - alpha*grad[i]
			print 'new theta: ' + str(current_theta[i])




	new_theta=current_theta

	return new_theta
