'''

Functions 


X, y = ML_EX2_Data1()
Andrew Ng's Data for EX2 from Machine Learning Training Set 1

X, y = ML_EX2_Data2()
Andrew Ng's Data for EX2 from Machine Learning Training Set 2

X, y = beacon_data()
Full pull from beacon database

headers = beacon_data_headers()
Pull the headers for the beacon database

X, y = beacon_data_simple()
Simple version of beacon database

headers = beacon_data_simple_headers()
Headers for the beacon simple data

X = add_ones_col(X)
Adds a one columns in the first column, this is used for some ML algorithms

X_norm, norm_vals = normalize(X)
Returns normalized array between -1 and 1 and normalization parameters

'''
import os
import sys
sys.path.insert(0, '../')

import csv
import numpy as np

import run#My Function -> Should probably rename this

def ML_EX2_Data1():
	'''Retruns the ML EX2 Data1 set'''
	y = []
	X = []
	data = []

	path = os.getcwd() + '/data/ex2data1.txt'


	with open(path, 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			data.append(row)

	data = np.array(data)


	X = data[:,0:data.shape[1]-1]
	y = data[:,(data.shape[1]-1):data.shape[1]]

	X = X.astype(np.float)
	y = y.astype(np.float)

	return X, y

def ML_EX2_Data2():
	'''Retruns the ML EX2 Data2 set'''
	y = []
	X = []
	data = []

	path = os.getcwd() + '/data/ex2data2.txt'


	with open(path, 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			data.append(row)

	data = np.array(data)


	X = data[:,0:data.shape[1]-1]
	y = data[:,(data.shape[1]-1):data.shape[1]]

	X = X.astype(np.float)
	y = y.astype(np.float)

	return X, y



def beacon_data():
	'''Full pull from beacon database

	Note: Number of variables is purposefully limited in this pull,
	much too much of a pain to correctly label and extract all data,
	particularly as end states are not known

	'''

	X = []
	y = []

	#Query SQL Database and pull all deals
	deals = run.Deal.query.all()


	for deal in deals:
			fee = int(deal.deal_fee)
			risk = deal.company_risk_rating
			debt_at_close = deal.debt_at_close
			pricing_score = deal.pricing_score

			risk_int = 0 #Dummy
			risk_int = float(deal.company_risk_rating)

			'''
			if risk == '1A':
				risk_int = 1
			elif risk == '1B':
				risk_int = 2
			elif risk == '1C':
				risk_int = 3    
			elif risk == '2A':
				risk_int = 4
			elif risk == '2B':
				risk_int = 5
			elif risk == '2C':
				risk_int = 6
			elif risk == '3A':
				risk_int = 7    
			elif risk == '3B':
				risk_int = 8
			elif risk == '3C':
				risk_int = 9
			elif risk == '4A':
				risk_int = 10
			elif risk == '4B':
				risk_int = 11   
			elif risk == '4C':
				risk_int = 12
			elif risk == '5A':
				risk_int = 13
			elif risk == '5B':
				risk_int = 14   
			elif risk == '5C':
				risk_int = 15
			else:
				risk_int = 16
			'''
			if deal.deal_status == "Win":
				y.append([1.0])
			else:
				y.append([0.0])

			X.append([risk_int,fee,debt_at_close,pricing_score])

	#Change inputs to intergers...... What a PAIN
	#For Testing we should keep this chill
	#Convert to Numpy Arrays

	X = np.array(X)
	y = np.array(y)

	X = X.astype(np.float)
	y = y.astype(np.float)


	return X, y

def beacon_data_headers():
	'''Just to keep track of what the database is outputting'''
	headers = ['Risk Rating','Closing Fee','Debt at Close']
	return headers


'''

X, y = beacon_data_simple()
Simple version of beacon database

headers = beacon_data_simple_headers()
Headers for the beacon simple data
'''

def add_ones_col(X):
	ones = np.ones((X.shape[0],1))
	temp = np.concatenate((ones, X), axis=1)

	return temp

def normalize(X):
	'''Returns normalized matrix with all values between -1 and 1, also returns what the matrix has been normalized by.'''
	#X, y = beacon_data()

	X_norm = []
	norm_vals = [1.0] * X.shape[1]
	norm_vals = np.array(norm_vals)

	#Create vector containing 0s, this will be filled with absolte max values for each column in X
	abs_max = [0.0] * X.shape[1]
	abs_max = np.array(abs_max)

	#print X

	
	for i in range(X.shape[1]):
		temp_max = np.amax(X[:,i])
		temp_min = np.amin(X[:,i])
		if abs(temp_max) > abs(temp_min):
			abs_max[i] = abs(temp_max)
		else:
			abs_max[i] = abs(temp_min)
	
	for i in range(len(abs_max)):
		if abs_max[i]>1:
			norm_vals[i] = abs_max[i]

	X_norm = np.divide(X,norm_vals)


	return X_norm, norm_vals








