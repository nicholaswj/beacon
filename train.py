'''

train.py


this module trains the model and saves it for use by the website


In the future there should be more options on what model to train, etc.

'''
import sys
sys.path.insert(0, './mlearn')
import log_reg1
import data_extract
import run
import numpy as np
import scipy.optimize

#Prediction helper function
def predict(theta, X):

	probability = log_reg1.sigmoid(np.dot(X,theta.T))

	return [1 if x >= 0.5 else 0 for x in probability]

#Check how many are correct
def correct(predictions, y):
	count = 0.0
	for i in range(y.shape[0]):
		if y[i] == predictions[i]:
			count = count + 1
	
	return count


def train():
		
	print('')
	print('')
	print('Beacon Dataset')
	print('Basic Test')

	degree = 1

	X,y = data_extract.beacon_data()
	X = data_extract.add_ones_col(X)


	#Map to higher features
	#degree = 4
	#X = log_reg1.map_feature(X[:,1],X[:,2],degree)
	X, norm = data_extract.normalize(X)
	theta = np.zeros(X.shape[1])
	result = scipy.optimize.fmin_tnc(func=log_reg1.cost, x0=theta, fprime=log_reg1.gradient, args=(X, y))
	theta_min=result[0]

	predictions = np.array(predict(theta_min, X))
	score = correct(predictions, y)
	print ('Test with a : ' + str(degree) + ' degree polynomial had ' +str(int(score)) + ' of ' 
		+ str(y.shape[0]) + ' or ' + str(score/float(y.shape[0])*100)[:5] + '% were correctly estimated on the Beacon dataset')


	#Output trained vars for use by flask  -------------------------------------------------------------------------------------

	output_predictions = log_reg1.sigmoid(np.dot(X,theta_min))

	model = 'Logistic Regression'
	msg = str(int(score)) + ' of ' + str(y.shape[0]) + ' (' + str(score/float(y.shape[0])*100)[:5] +'%)'


	#This really should be in a db or something, text file for now :)
	f = open('model.csv', 'w')

	f.write(model+'\n')  # python will convert \n to os.linesep
	f.write(msg+'\n')  # python will convert \n to os.linesep
	f.write(','.join(map(str, theta_min.tolist()))+'\n')
	f.write(','.join(map(str, norm.tolist()))+'\n')

	f.close()  

	