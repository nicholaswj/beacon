'''
To play around with graphing and math libraries

Goal is to get to a logistic regression

'''

import run
from run import db
import matplotlib.pyplot as plt


graph_me = []
y_win=[]
x_win=[]
y_loss = []
x_loss = []

deals = run.Deal.query.all()

for deal in deals:
	fee = int(deal.deal_fee)
	risk = deal.company_risk_rating
	risk_int = 0

	if risk == '1A':
		risk_int = 1
	elif risk == '1B':
		risk_int = 2
	elif risk == '1C':
		risk_int = 3	
	elif risk == '2A':
		risk_int = 4
	elif risk == '2B':
		risk_int = 5
	elif risk == '2C':
		risk_int = 6
	elif risk == '3A':
		risk_int = 7	
	elif risk == '3B':
		risk_int = 8
	elif risk == '3C':
		risk_int = 9
	elif risk == '4A':
		risk_int = 10
	elif risk == '4B':
		risk_int = 11	
	elif risk == '4C':
		risk_int = 12
	elif risk == '5A':
		risk_int = 13
	elif risk == '5B':
		risk_int = 14	
	elif risk == '5C':
		risk_int = 15
	else:
		risk_int = 16

	if deal.deal_status == "Win":
		y_win.append(fee)
		x_win.append(risk_int)

	else:
		y_loss.append(fee)
		x_loss.append(risk_int)


'''
plt.plot(x_win,y_win,'ro',label='Win')
plt.plot(x_loss,y_loss,'bo',label='Loss')

#plt.tight_layout()
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

plt.show()
'''

fig = plt.figure()
ax = plt.subplot(111)

#plt.ylabel('Closing Fee')
plt.xlabel('Risk Rating')
plt.ylabel('Closing Fee')

ax.plot(x_win,y_win,'ro',label='Win')
ax.plot(x_loss,y_loss,'bo',label='Loss')

# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0 + box.width * 0.1, box.y0, box.width * 0.8, box.height])

# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

plt.show()






