'''
Let's just imagine the front end

'''
#To enable mlearn folder to be added
import sys
sys.path.insert(0, './mlearn')
import log_reg1
import numpy as np

from flask import Flask, request, render_template, json, session, redirect, url_for, escape
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func

from datetime import datetime
import os, requests, random

import csv

app = Flask(__name__)


#Randomly generated for signing cookies
app.secret_key = 'A0Zr3243298j/3yX R~XHH!jmN]LWX/,?RT'



#-------------------------- SQL Alchemy Config
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = 'sqlite:///' + os.path.join(basedir, 'app.db')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_MIGRATE_REPO'] = os.path.join(basedir, 'db_repository')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#--------------------------- SQL Model Section

db = SQLAlchemy(app)

#Deal Data Structure - Should be moved into it's own file at some point
class Deal(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	deal_date = db.Column(db.DateTime)
	deal_status = db.Column(db.String(120))
	deal_status_comment = db.Column(db.String(300))
	deal_source = db.Column(db.String(100))
	deal_source_comment = db.Column(db.String(300))
	deal_fee = db.Column(db.Integer)

	company_name = db.Column(db.String(100))
	company_id = db.Column(db.String(100))
	company_revenue = db.Column(db.String(100))
	company_employees = db.Column(db.String(100))
	company_current_client = db.Column(db.String(100))
	company_competitive = db.Column(db.String(100))
	company_industry = db.Column(db.String(100))
	company_risk_rating = db.Column(db.String(100))
	company_ownership = db.Column(db.Text)

	company_geo_country = db.Column(db.String(100))
	company_geo_province = db.Column(db.String(100))
	company_geo_city = db.Column(db.String(100))

	branch_name = db.Column(db.String(100))
	branch_id = db.Column(db.String(100))
	branch_people_1 = db.Column(db.String(100))
	branch_people_2 = db.Column(db.String(100))
	branch_people_3 = db.Column(db.String(100))

	facility_size = db.Column(db.String(100))
	facility_committed = db.Column(db.String(100))
	facility_term = db.Column(db.String(100))
	facility_syndicated = db.Column(db.String(100))

	pricing_level = db.Column(db.String(100))
	pricing_options = db.Column(db.String(100))

	pricing_1_1 = db.Column(db.String(100))
	pricing_1_2 = db.Column(db.String(100))
	pricing_1_3 = db.Column(db.String(100))
	pricing_1_4 = db.Column(db.String(100))
	pricing_2_1 = db.Column(db.String(100))
	pricing_2_2 = db.Column(db.String(100))
	pricing_2_3 = db.Column(db.String(100))
	pricing_2_4 = db.Column(db.String(100))
	pricing_3_1 = db.Column(db.String(100))
	pricing_3_2 = db.Column(db.String(100))
	pricing_3_3 = db.Column(db.String(100))
	pricing_3_4 = db.Column(db.String(100))
	pricing_4_1 = db.Column(db.String(100))
	pricing_4_2 = db.Column(db.String(100))
	pricing_4_3 = db.Column(db.String(100))
	pricing_4_4 = db.Column(db.String(100))
	pricing_5_1 = db.Column(db.String(100))
	pricing_5_2 = db.Column(db.String(100))
	pricing_5_3 = db.Column(db.String(100))
	pricing_5_4 = db.Column(db.String(100))

	pricing_score = db.Column(db.Integer)

	covenant_1_type = db.Column(db.String(100))
	covenant_1_test = db.Column(db.String(100))
	covenant_2_type = db.Column(db.String(100))
	covenant_2_test = db.Column(db.String(100))

	debt_at_close = db.Column(db.String(100))

	file_deal_memo = db.Column(db.String(100))
	file_deal_terms = db.Column(db.String(100))


	def __repr__(self):
		return '<Deal # %r>' % (self.id)


@app.route('/test')
def test():
	if 'username' not in session:
		return redirect(url_for('index'))
	return 'Logged in as %s' % escape(session['username'])

@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'POST' and request.form['username'].lower() == 'beacon' and request.form['password'] == 'bacon':
		session['username'] = request.form['username']
		return redirect(url_for('pricing_lookup'))
	return render_template('index.html')

@app.route('/logout')
def logout():
	# remove the username from the session if it's there
	session.pop('username', None)
	return redirect(url_for('index'))

@app.route('/deal_database/')
def deal_db():
	if 'username' not in session:
		return redirect(url_for('index'))
	deals = Deal.query.all()
	return render_template('deal_db.html',deals=deals)

@app.route('/result/')
def result():
	if 'username' not in session:
		return redirect(url_for('index'))

	return render_template('result.html')

@app.route('/upload_deal/')
def upload_deal():
	if 'username' not in session:
		return redirect(url_for('index'))

	return render_template('upload_deal.html')

@app.route('/analytics/')
def analytics():
	if 'username' not in session:
		return redirect(url_for('analytics'))
	deals = Deal.query.all()
	return render_template('analytics.html', deals=deals)

@app.route('/db_test/')
def db_test():

	deals = Deal.query.all()

	return render_template('db_test.html',deals=deals)

@app.route('/inspect/', methods=['GET'], strict_slashes=False)
def inspect():
	if 'username' not in session:
		return redirect(url_for('index'))

	deal = None
	deal_id = request.args.get('deal_id')
	print(deal_id)
	deals = Deal.query.all()

	for temp_deal in deals:
		if str(temp_deal.id) == str(deal_id):
			deal = temp_deal

	if deal == None:
		return render_template('not_found.html',data=deal_id)
	else:
		return render_template('inspect.html',deal=deal)


@app.route('/estimate_page/', methods=['GET'], strict_slashes=False)
def estimate_page():
	'''Take GET inputs and return prediction'''

	#check if user is logged in
	if 'username' not in session:
		return redirect(url_for('index'))

	#pull out the GET values
	deal_fee = request.args.get('deal_fee')
	company_risk_rating = request.args.get('company_risk_rating')
	debt_at_close = request.args.get('debt_at_close')
	#Pull pricing Grid, yeah this is a shitty way to store vars and really shitty not checking for text input
	try:
		debt_1 = float(request.args.get('debt_1'))
		prime_1 = float(request.args.get('prime_1'))
		debt_2 = float(request.args.get('debt_2'))
		prime_2 = float(request.args.get('prime_2'))
		debt_3 = float(request.args.get('debt_3'))
		prime_3 = float(request.args.get('prime_3'))
		debt_4 = float(request.args.get('debt_4'))
		prime_4 = float(request.args.get('prime_4'))
		debt_5 = float(request.args.get('debt_5'))
		prime_5 = float(request.args.get('prime_5'))
	except:
		debt_1 = None
		prime_1 = None
		debt_2 = None
		prime_2 = None
		debt_3 = None
		prime_3 = None
		debt_4 = None
		prime_4 = None
		debt_5 = None
		prime_5 = None

	#fill in the none-data with dummy values
	if deal_fee == None:
		deal_fee = 153.0
	if company_risk_rating == None:
		company_risk_rating = 8
	if debt_at_close == None:
		debt_at_close = 2

	if debt_1 == None:
		debt_1 = 1.00
	if prime_1 == None:
		prime_1 = 50
	if debt_2 == None:
		debt_2 = 1.75
	if prime_2 == None:
		prime_2 = 75
	if debt_3 == None:
		debt_3 = 2.50
	if prime_3 == None:
		prime_3 = 100
	if debt_4 == None:
		debt_4 = 3.25
	if prime_4 == None:
		prime_4 = 125
	if debt_5 == None:
		debt_5 = 3.25
	if prime_5 == None:
		prime_5 = 150

	#Calculate pricing score
	pricing_score_max_debt_EBITDA = 4
	pricing_score = debt_1*prime_1 + (debt_2-debt_1)*prime_2 + \
		(debt_3-debt_2)*prime_3 + (debt_4-debt_3)*prime_4 + \
		(pricing_score_max_debt_EBITDA-debt_5)*prime_5

	#print(pricing_score)

	#input used to calculate chance of winning
	deal_input = [1,company_risk_rating,deal_fee,debt_at_close,pricing_score]


	with open('model.csv') as csvfile:
		model_type = next(csvfile)
		win_rate = next(csvfile)
		temp_theta = next(csvfile)[0:-1]
		temp_norm = next(csvfile)[0:-1]

	temp_theta = temp_theta.split(',')
	temp_norm = temp_norm.split(',')

	theta = temp_theta
	deal_norm = temp_norm
	
	#convert to arrays of type float
	deal_input = np.array(deal_input)
	deal_input = deal_input.astype(np.float)
	theta = np.array(theta)
	theta = theta.astype(np.float)
	deal_norm = np.array(deal_norm)
	deal_norm = deal_norm.astype(np.float)


	#Normalize deal_input
	deal_input = deal_input/deal_norm

	estimate = log_reg1.sigmoid(np.dot(deal_input,theta))*100.0

	print(theta)
	print(deal_input)
	print(log_reg1.sigmoid(np.dot(deal_input,theta)))

	if estimate < 0.0001:
		estimate = 0.0

	if estimate >= 50.0:
		win = True
	else:
		win = False	

	data = {'estimate':str(estimate)[0:4],'win':win,'win_rate':win_rate,'model_type':model_type}
	inputs = {'deal_fee':deal_fee,'company_risk_rating':str(company_risk_rating),'debt_at_close':debt_at_close,
		'debt_1':debt_1,'prime_1':prime_1,'debt_2':debt_2,'prime_2':prime_2,'debt_3':debt_3,'prime_3':prime_3,
		'debt_4':debt_4,'prime_4':prime_4,'debt_5':debt_5,'prime_5':prime_5,}
	

	return render_template('estimate_page.html',data=data,inputs=inputs)

@app.route('/pricing_lookup/', methods=['GET'], strict_slashes=False)
@app.route('/main/', methods=['GET'], strict_slashes=False)
def pricing_lookup():
	'''Take GET inputs and estimated market pricing'''

	#Used to convert alphanumeric risk rating to number value

	#check if user is logged in
	if 'username' not in session:
		return redirect(url_for('index'))

	#pull out the GET values
	confidence = request.args.get('confidence')
	company_risk_rating = request.args.get('company_risk_rating')
	debt_at_close = request.args.get('debt_at_close')


	#fill in the none-data with dummy values
	if confidence == None:
		confidence = 50.0
	if company_risk_rating == None:
		company_risk_rating = 5
	if debt_at_close == None:
		debt_at_close = 2

	confidence = float(confidence)
	company_risk_rating = int(company_risk_rating)
	#deal_input = [1,company_risk_rating,deal_fee,debt_at_close]


	with open('model.csv') as csvfile:
		model_type = next(csvfile)
		win_rate = next(csvfile)
		temp_theta = next(csvfile)[0:-1]
		temp_norm = next(csvfile)[0:-1]

	temp_theta = temp_theta.split(',')
	temp_norm = temp_norm.split(',')

	theta = temp_theta
	deal_norm = temp_norm
	'''
	#convert to arrays of type float
	deal_input = np.array(deal_input)
	deal_input = deal_input.astype(np.float)
	theta = np.array(theta)
	theta = theta.astype(np.float)
	deal_norm = np.array(deal_norm)
	deal_norm = deal_norm.astype(np.float)

	#Normalize deal_input
	deal_input = deal_input/deal_norm

	estimate = log_reg1.sigmoid(np.dot(deal_input,theta))*100.0

	if estimate >= 50.0:
		win = True
	else:
		win = False	
	'''

	'''
	Our estimate takes the average closing_fee for a certain risk level and then generates the appropriate pricing grid
	This is a big of half assed effort, this simple uses a linear relationship between pricing_score and closing_fee
	It should really be done with something that adjusts both values, will be better when quadratic relationships are modelled
	'''
	#print(Deal.query.all())
	#still some variation, take the value of current level and one below
	if company_risk_rating != 15:
		offset = int(company_risk_rating) + 1
		avg_closing_fee1=Deal.query.with_entities(func.avg(Deal.deal_fee).label('average')).filter(Deal.company_risk_rating==company_risk_rating).all()
		avg_closing_fee2=Deal.query.with_entities(func.avg(Deal.deal_fee).label('average')).filter(Deal.company_risk_rating==offset).all()
		avg_closing_fee = (avg_closing_fee1[0][0]+avg_closing_fee2[0][0])/2
	else:
		avg_closing_fee=Deal.query.with_entities(func.avg(Deal.deal_fee).label('average')).filter(Deal.company_risk_rating==company_risk_rating).all()
		avg_closing_fee = avg_closing_fee[0][0]

	closing_fee = avg_closing_fee

	with open('model.csv') as csvfile:
		model_type = next(csvfile)
		win_rate = next(csvfile)
		temp_theta = next(csvfile)[0:-1]
		temp_norm = next(csvfile)[0:-1]

	temp_theta = temp_theta.split(',')
	temp_norm = temp_norm.split(',')

	theta = temp_theta
	deal_norm = temp_norm

	#2 and 4
	#2 is pricing fee
	#4 is pricing_score
	#Yeah this whole thing is a bit shoddy
	Y = float(theta[0]) + float(theta[1])*float(company_risk_rating)/float(deal_norm[1]) + float(theta[2])*float(avg_closing_fee)/float(deal_norm[2]) + float(theta[3])*float(debt_at_close)/float(deal_norm[3])
	pricing_score = (confidence/100.0 - Y)/float(theta[4])

	pricing_score = pricing_score * float(deal_norm[4])

	'''Now that we have pricing_score move it into grid format, assume standard debt levels'''
	
	base_pricing = (pricing_score - 187.5)/4.0

	pricing_1_1 = 1
	pricing_1_2 = 1.0
	pricing_1_3 = base_pricing
	pricing_1_4 = pricing_1_3 + 100.0

	pricing_2_1 = 2
	pricing_2_2 = 1.75
	pricing_2_3 = base_pricing + 25.0
	pricing_2_4 = pricing_2_3 + 100.0

	pricing_3_1 = 3
	pricing_3_2 = 2.5
	pricing_3_3 = base_pricing + 50.0
	pricing_3_4 = pricing_3_3 + 100.0

	pricing_4_1 = 4
	pricing_4_2 = 3.25
	pricing_4_3 = base_pricing + 75.0
	pricing_4_4 = pricing_4_3 + 100.0

	pricing_5_1 = 5
	pricing_5_2 = 3.25
	pricing_5_3 = base_pricing + 100.0
	pricing_5_4 = pricing_5_3 + 100.0


	grid = {'pricing_1_1':pricing_1_1,'pricing_1_2':pricing_1_2,'pricing_1_3':pricing_1_3,'pricing_1_4':pricing_1_4,
	'pricing_2_1':pricing_2_1,'pricing_2_2':pricing_2_2,'pricing_2_3':pricing_2_3,'pricing_2_4':pricing_2_4,
	'pricing_3_1':pricing_3_1,'pricing_3_2':pricing_3_2,'pricing_3_3':pricing_3_3,'pricing_3_4':pricing_3_4,
	'pricing_4_1':pricing_4_1,'pricing_4_2':pricing_4_2,'pricing_4_3':pricing_4_3,'pricing_4_4':pricing_4_4,
	'pricing_5_1':pricing_5_1,'pricing_5_2':pricing_5_2,'pricing_5_3':pricing_5_3,'pricing_5_4':pricing_5_4}
	data = {'closing_fee':closing_fee,'win_rate':win_rate,'model_type':model_type}
	inputs = {'confidence':confidence,'company_risk_rating':str(company_risk_rating),'debt_at_close':debt_at_close}
	

	return render_template('pricing_lookup.html',data=data,inputs=inputs,grid=grid)

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=3000, threaded=True)


