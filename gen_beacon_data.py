'''
gen_beacon_data.py


Generate Beacon Data
Function to clean split the generating of data outside of the main Flask function

date = generate_date()

country, province, city = generate_geo()

name = generate_company_name()



'''
import run
from run import db
import random
from datetime import datetime


def generate_date():
	date = datetime(2016,random.randint(1,12),random.randint(1,29))
	return date

def generate_geo():
	cities = ['Toronto','Ottawa','Hamilton','Waterloo','London','St-Catherines','Guelph']

	country = 'Canada'
	province = 'Ontario'
	city = random.choice(cities)

	return country, province, city

def generate_company_name():
	#Name Generation -------------------------------------
	company_acronym = ['Ltd.','Inc.','Partners','LLC']

	'''
	word_site = 'http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain'

	#In case this is being tested without internet, catch the no connection error
	try:
		response = requests.get(word_site)
		words = response.content.splitlines()
	except:
		words = ['Offline_1','Offline_2','Offline_3','Offline_4']
	'''

	f = open('words.txt', 'r')
	words = f.readlines()
	for i in range(len(words)-1):
		words[i] = words[i][:len(words[i])-1]
	f.close()


	name = random.choice(words) + ' ' + random.choice(company_acronym)
	name = name[0].upper() + name[1:]
	return name


def generate_deal():

	#Input variables for generator
	#assume all terms are 5 for now
	facility_term = 5
	win_percentage = 50 #in, whole #s only
	deal_fee_input = 15.0*facility_term #Input, basis points per year
	deal_fee_variation = deal_fee_input*0.1 #just a guess to give #s some flavour

	#These are just straight constant variables right now
	pricing_level = '3' #this is the number of levels based on debt levels
	pricing_options = '3' #this is the pricing options 3 for now Prime +, LIBOR + and BA Stamping Fee

	#Ownership
	owners = ['private','public','financial']
	company_ownership = random.choice(owners)

	#Lists to select from
	risk_rating_list = ['1A','1B','1C','2A','2B','2C','3A','3B','3C','4A','4B','4C','5A','5B','5C','6']
	risk_rating_list_modifier = {'1A':0,'1B':1,'1C':2,'2A':3,'2B':4,'2C':5,'3A':6,'3B':7,'3C':8,'4A':9,'4B':10,'4C':11,'5A':12,'5B':13,'5C':14,'6':15}

	#Base Version Generator


	#This dummy data is rather dumb right now, need to tie it together in a smart way
	#Generator Functions


	company_name = generate_company_name()
	deal_date = generate_date()
	company_geo_country, company_geo_province, company_geo_city = generate_geo()

	#One off generation
	if random.randint(0,100) <= win_percentage:
		deal_status = 'Win'
	else:
		deal_status = 'Loss'

	company_risk_rating = random.choice(risk_rating_list)
	company_risk_rating = risk_rating_list_modifier[company_risk_rating]


	'''		
	Add a little character to Deal fee
	This just upps the deal fee and variation of the fee by 5% for every increase in risk level and 10% if loss
	'''		
	#Modify Deal_fee by risk rating
	modifier = float(company_risk_rating)*5.0/100.0
	deal_fee_input_modified = float(deal_fee_input)*(1.0+modifier)
	deal_fee_variation_modified = float(deal_fee_variation)*(1.0+modifier)

	deal_fee = int(random.normalvariate(deal_fee_input_modified,deal_fee_variation_modified))
	if deal_status == 'Loss':
		deal_fee = deal_fee * 1.1

	#Debt at close random between 0x and 4.5x, double deal fee is 4.5x and linear decrease to 0 mod at 0x 
	debt_at_close = float(random.randint(0,45))/10.0

	deal_fee = deal_fee * (1.0+debt_at_close/4.5)
	deal_fee = round(deal_fee)


	#Pricing Grid, very similar process to closing fee
	#Using set levels of 5 and set Debt:EBITDA pricing

	base_pricing = company_risk_rating * 5.0

	if deal_status == 'Loss':
		base_pricing = base_pricing + 15 #15bps premium

	#add a little uncertainty into the mix
	base_pricing=float(random.normalvariate(base_pricing,10))

	pricing_1_1 = 1
	pricing_1_2 = 1.0
	pricing_1_3 = base_pricing
	pricing_1_4 = pricing_1_3 + 100.0

	pricing_2_1 = 2
	pricing_2_2 = 1.75
	pricing_2_3 = base_pricing + 25.0
	pricing_2_4 = pricing_2_3 + 100.0

	pricing_3_1 = 3
	pricing_3_2 = 2.5
	pricing_3_3 = base_pricing + 50.0
	pricing_3_4 = pricing_3_3 + 100.0

	pricing_4_1 = 4
	pricing_4_2 = 3.25
	pricing_4_3 = base_pricing + 75.0
	pricing_4_4 = pricing_4_3 + 100.0

	pricing_5_1 = 5
	pricing_5_2 = 3.25
	pricing_5_3 = base_pricing + 100.0
	pricing_5_4 = pricing_5_3 + 100.0

	#generate pricing score to calculate area under the graph for prime+, assuming grid goes up to 4x debt:EBITDA
	pricing_score_max_debt_EBITDA = 4
	pricing_score = pricing_1_2*pricing_1_3 + (pricing_2_2-pricing_1_2)*pricing_2_3 + \
		(pricing_3_2-pricing_2_2)*pricing_3_3 + (pricing_4_2-pricing_3_2)*pricing_4_3 + \
		(pricing_score_max_debt_EBITDA-pricing_5_2)*pricing_5_3


	#Create the Deal and add it to the session
	u = run.Deal(deal_date=deal_date,
		deal_status=deal_status,
		deal_fee=deal_fee,
		company_name=company_name,
		company_geo_country=company_geo_country,
		company_geo_province=company_geo_province,
		company_geo_city=company_geo_city,
		company_risk_rating=company_risk_rating,
		company_ownership=company_ownership,
		pricing_level=pricing_level,
		pricing_options=pricing_options,
		debt_at_close=debt_at_close,
		pricing_1_1 = pricing_1_1,
		pricing_1_2 = pricing_1_2,
		pricing_1_3 = pricing_1_3,
		pricing_1_4 = pricing_1_4,

		pricing_2_1 = pricing_2_1,
		pricing_2_2 = pricing_2_2,
		pricing_2_3 = pricing_2_3,
		pricing_2_4 = pricing_2_4,

		pricing_3_1 = pricing_3_1,
		pricing_3_2 = pricing_3_2,
		pricing_3_3 = pricing_3_3,
		pricing_3_4 = pricing_3_4,

		pricing_4_1 = pricing_4_1,
		pricing_4_2 = pricing_4_2,
		pricing_4_3 = pricing_4_3,
		pricing_4_4 = pricing_4_4,

		pricing_5_1 = pricing_5_1,
		pricing_5_2 = pricing_5_2,
		pricing_5_3 = pricing_5_3,
		pricing_5_4 = pricing_5_4,

		pricing_score = pricing_score)
	return u

def deals_to_db(deals_to_add_input):

	#Pull data from GET on how many deals to generate

	deals_to_add = int(deals_to_add_input)

	#Output Array
	added_deals=[] #array to hold all deals added
	#Base Version Generator
	for i in range(deals_to_add):

		#Update the terminal for monitoring purposes
		print('Generating Deal ' + str(i) + ' of ' + str(deals_to_add))


		#Create the Deal and add it to the session
		u = generate_deal()
		db.session.add(u)
		added_deals.append(u)

	db.session.commit()

	msg = str(deals_to_add) + ' deals have been added to the db'

	return msg

